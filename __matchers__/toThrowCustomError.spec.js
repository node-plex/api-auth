'use strict'

//import matcher from './toThrowCustomError'
const toThrowCustomError = require('./toThrowCustomError')

expect.extend({toThrowCustomError})

class CustomError extends Error {
  constructor (message, code) {
    super(message)

    return Object.assign(this, {message, code})
  }
}

describe('toThrowCustomError Jest matcher', () => {
  it('throws if callback is not provided', () => {
    // eslint-disable-next-line jest/valid-expect
    expect(_ => expect().toThrowCustomError())
      .toThrowErrorMatchingSnapshot()
  })

  it('throws when callback is not a Function', () => {
    expect(_ => expect(null).toThrowCustomError())
      .toThrowErrorMatchingSnapshot()
  })

  it('throws when expected Error is not provided', () => {
    expect(_ => expect(_ => null).toThrowCustomError())
      .toThrowErrorMatchingSnapshot()
  })

  it('throws when expected Error is not an Error instance', () => {
    expect(_ => expect(_ => null).toThrowCustomError({message: 'not an error'}))
      .toThrowErrorMatchingSnapshot()
  })

  it('fails when callback does not throw', () => {
    expect(_ => expect(_ => null).toThrowCustomError(new CustomError))
      .toThrowErrorMatchingSnapshot()
  })

  it('fails when thrown and expected Error classes are not equal', () => {
    expect(_ => expect(_ => { throw new CustomError('Message') })
      .toThrowCustomError(new TypeError('Message')))
        .toThrowErrorMatchingSnapshot()
  })

  it('fails when thrown and expected Error messages are not equal', () => {
    expect(_ => expect(_ => { throw new RangeError('Message') })
      .toThrowCustomError(new RangeError('message')))
        .toThrowErrorMatchingSnapshot()
  })

  it('fails when thrown and expected Error codes are not equal', () => {
    expect(_ => expect(_ => { throw new CustomError('Msg', 'ERR_CUSTOM_TEST') })
      .toThrowCustomError(new CustomError('Msg')))
        .toThrowErrorMatchingSnapshot()
    expect(_ => expect(_ => { throw new CustomError('Msg', 'ERR_CUSTOM_TEST') })
      .toThrowCustomError(new CustomError('Msg', 'ERR_INVALID_OPTION')))
        .toThrowErrorMatchingSnapshot()
  })

  it('passes when thrown and expected Errors are equal', () => {
    expect(_ => expect(_ => { throw new CustomError('Msg', 'ERR_CUSTOM_TEST') })
      .toThrowCustomError(new CustomError('Msg', 'ERR_CUSTOM_TEST')))
        .not.toThrow()
  })

  describe('when used with NOT predicate', () => {
    it('fails when thrown and expected Errors are equal', () => {
      expect(_ => expect(_ => { throw new RangeError('Message') })
        .not.toThrowCustomError(new RangeError('Message')))
          .toThrowErrorMatchingSnapshot()
    })

    it('passes when thrown and expected Errors are not equal', () => {
      expect(_ => expect(_ => { throw new RangeError('Message') })
        .not.toThrowCustomError(new RangeError('message')))
          .not.toThrow()
    })
  })
})
