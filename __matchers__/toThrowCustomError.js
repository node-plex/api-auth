'use strict'

function toThrowCustomError (received, expected) {
  const matcherName = 'toThrowCustomError',
        options = {
          isNot: this.isNot,
          promise: this.promise,
        },
        {
          EXPECTED_COLOR,
          RECEIVED_COLOR,
          matcherErrorMessage,
          matcherHint,
          printDiffOrStringify,
          printExpected,
          printReceived,
          printWithType,
          stringify
        } = this.utils,
        hint = _ => `${matcherHint(
          matcherName, 'Function', expected.constructor.name, options
        )}\n\n`
  let message = _ => `${hint()}`
      + `Expected Error: ${EXPECTED_COLOR(`NOT ${expected.constructor.name}`)}`
      + `\nReceived Error: ${RECEIVED_COLOR(stringify({
        code: thrown.code,
        constructor: thrown.constructor.name,
        message: thrown.message,
      }))}`,
      pass = false,
      thrown = null

  if (typeof received !== 'function') {
    const placeholder = received === undefined ? '' : 'received'
    throw new Error(
      matcherErrorMessage(
        matcherHint(matcherName, placeholder, '', options),
        `${RECEIVED_COLOR('received')} value must be a function`,
        printWithType('Received', received, printReceived),
      ),
    )
  }
  if (!(expected instanceof Error)) {
    const placeholder = expected === undefined ? '' : 'expected'
    throw new Error(
      matcherErrorMessage(
        matcherHint(matcherName, 'Function', placeholder, options),
        `${EXPECTED_COLOR('expected')} value must be an Error instance`,
        printWithType('Expected', expected, printExpected),
      ),
    )
  }

  try { received() } catch (e) { thrown = e }

  if (thrown === null) {
    message = _ => `${hint()}Received function did not throw`
  } else if (!(thrown instanceof expected.constructor)) {
    message = _ => `${hint()}`
      + `${printDiffOrStringify(
        expected.constructor.name,
        thrown.constructor.name,
        'Expected Error constructor',
        'Received Error constructor',
        this.expand,
      )}`
  } else if (thrown.message !== expected.message) {
    message = _ => `${hint()}`
      + `${printDiffOrStringify(
        expected.message,
        thrown.message,
        'Expected Error message',
        'Received Error message',
        this.expand,
      )}`
  } else if (thrown.code !== expected.code) {
    message = _ => `${hint()}`
      + `${printDiffOrStringify(
        expected.code,
        thrown.code,
        'Expected Error code',
        'Received Error code',
        this.expand,
      )}`
  } else {
    pass = true
  }

  return {message, pass}
}

//export default toThrowCustomError
module.exports = toThrowCustomError
