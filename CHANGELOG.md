# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.8](https://gitlab.com/node-plex/plex-api-auth/compare/v0.0.7...v0.0.8) (2020-04-21)

### [0.0.7](https://gitlab.com/node-plex/plex-api-auth/compare/v0.0.6...v0.0.7) (2020-04-21)

### [0.0.6](https://gitlab.com/node-plex/plex-api-auth/compare/v0.0.5...v0.0.6) (2020-04-21)

### [0.0.5](https://gitlab.com/node-plex/plex-api-auth/compare/v0.0.4...v0.0.5) (2020-04-21)

### [0.0.4](https://gitlab.com/chet.manley/plex-app-authenticator/compare/v0.0.3...v0.0.4) (2020-04-18)

### [0.0.3](https://gitlab.com/chet.manley/plex-app-authenticator/compare/v0.0.2...v0.0.3) (2020-04-07)


### Features

* build-out functionality to satisfy tests ([f076b74](https://gitlab.com/chet.manley/plex-app-authenticator/commit/f076b74b0026d14c9efb332f64be34f6c926f179))


### Bug Fixes

* remove duplicate horizontal rule ([8445c24](https://gitlab.com/chet.manley/plex-app-authenticator/commit/8445c2459dca45d90d41999f9a26b32ea268cf0f))
* remove unsupported Kramdown from readme ([b062f88](https://gitlab.com/chet.manley/plex-app-authenticator/commit/b062f887900140e098765a5dd2dbd777574f4b06))

### 0.0.2 (2020-03-29)
