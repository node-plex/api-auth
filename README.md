# plex-api-auth

A Node.js library that retrieves an authentication token from Plex for use in applications. This project is based on the work found in [node-plex-api-headers](https://github.com/phillipj/node-plex-api-headers/) and [node-plex-api-credentials](https://github.com/phillipj/node-plex-api-credentials/). While influenced by the previous work, this rewrite aims to improve upon it in two major ways:

* Implement the most current official token retrieval method described [here](https://forums.plex.tv/t/how-to-request-a-x-plex-token-token-for-your-app/84551).
* Modernize and simplify the implementation with ES6 best practices.

## TODO
* [x] migrate project to GitLab group
* [x] rename repository plex-api-auth
* [x] implement integration branch
* [ ] automate `.gitlab-ci.yml` linting
* [ ] automate package publishing to GitLab & NPM
  * [ ] calculate package tag in pipeline
* [ ] finish tests
  * [ ] break-out index tests
* [ ] create CLI stub :grey_question:
* [ ] create a custom npm init <initializer> based on this repo
