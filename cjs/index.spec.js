'use strict'

const { default: PlexAppAuthenticator, APIError } = require('./index')

describe('PlexAppAuthenticator CJS module', () => {
  it('exports a PlexAppAuthenticator class', () => {
    expect(PlexAppAuthenticator.name).toBe('PlexAppAuthenticator')
  })

  it('exports an APIError class', () => {
    expect(new APIError).toBeInstanceOf(Error)
  })
})
