'use strict'

/*
 * Currently broken since Node versions ^12.16 and ^13.2.
 * https://github.com/standard-things/esm/issues/868
 * https://github.com/standard-things/esm/issues/855
 */
const requireESM = require('esm')(module)

module.exports = requireESM('../lib')
