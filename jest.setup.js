'use strict'

//import stripAnsi from 'strip-ansi'
const stripAnsi = require('strip-ansi')

//import matchers from './__matchers__'
const matchers = require('./__matchers__')

expect.extend(matchers)

expect.addSnapshotSerializer({
  print: value => `${stripAnsi(value)}`,
  test: value => value && typeof value === 'string',
})
