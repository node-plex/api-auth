'use strict'

const config = {
  APP_NAME: process.env.npm_package_name,
  APP_VERSION: process.env.npm_package_version,
  AUTH_ENDPOINT: 'https://plex.tv/users/sign_in.json',
  REQUIRED: {
    headers: [
      'X-Plex-Client-Identifier',
      'X-Plex-Product',
      'X-Plex-Version',
    ],
    options: [
      'headers',
      'password',
      'username',
    ],
  },
}

//export default config
module.exports = exports = config
module.exports.default = exports
