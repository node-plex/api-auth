'use strict'

//import fetch, { Headers } from 'node-fetch'
const { default: fetch, Headers } = require('node-fetch')

//import PlexAppAuthenticator, { APIError, ArgumentError } from '../index.js'
//import { ERRORS } from '../errors/index.js'
const {
  default: PlexAppAuthenticator,
  APIError,
  ArgumentError
} = require('../index')
const { ERRORS } = require('../errors')

//import * as config from './index.spec.json'
const config = require('./index.spec.json')

const getInstance = o => new PlexAppAuthenticator(o)
const goodOptions = {
  headers: config.request.headers.good,
  ...config.credentials.good
}

describe('PlexAppAuthenticator module', () => {
  it('exports a PlexAppAuthenticator class', () => {
    expect(PlexAppAuthenticator.name).toBe('PlexAppAuthenticator')
  })

  it('exports an APIError class', () => {
    expect(APIError.name).toBe('APIError')
  })

  it('exports an ArgumentError class', () => {
    expect(ArgumentError.name).toBe('ArgumentError')
  })
})

describe('PlexAppAuthenticator class constructor', () => {
  it('throws if `options` argument is missing', () => {
    expect(getInstance)
      .toThrowCustomError(new ArgumentError(ERRORS.missingOptionsArg))
  })

  it('throws if `options` Object is empty', () => {
    expect(() => getInstance({}))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingOptions,
        missing: config.required.options,
      }))
  })

  it('throws if `options` is not a native Object', () => {
    const arrayObj = [],
          falseObj = false,
          nullObj = null,
          stringObj = 'string',
          trueObj = true,
          error = new ArgumentError(ERRORS.missingOptionsArg)

    expect(() => getInstance(arrayObj)).toThrowCustomError(error)
    expect(() => getInstance(falseObj)).toThrowCustomError(error)
    // native TypeError, thrown by constructor
    expect(() => getInstance(nullObj)).toThrow(TypeError)
    expect(() => getInstance(stringObj)).toThrowCustomError(error)
    expect(() => getInstance(trueObj)).toThrowCustomError(error)
  })

  it('throws if any required options are missing', () => {
    const missingAll = {key: 'value'},
          noHeaders = config.credentials.good,
          noPassword = {
            headers: goodOptions.headers,
            username: goodOptions.username,
          },
          noUsername = {
            headers: goodOptions.headers,
            password: goodOptions.password,
          }

    expect(() => getInstance(missingAll))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingOptions,
        missing: config.required.options,
      }))
    expect(() => getInstance(noHeaders))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingOptions,
        missing: ['headers'],
      }))
    expect(() => getInstance(noPassword))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingOptions,
        missing: ['password'],
      }))
    expect(() => getInstance(noUsername))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingOptions,
        missing: ['username'],
      }))
  })

  it('throws if any required options are empty', () => {
    const emptyAll = {headers:new Headers, username:'', password:''},
          emptyHeaders = {headers:{}, ...config.credentials.good},
          emptyPassword = {
            headers: goodOptions.headers,
            username: goodOptions.username,
            password: '',
          },
          emptyUsername = {
            headers: goodOptions.headers,
            username: '',
            password: goodOptions.password,
          }

    expect(() => getInstance(emptyAll))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingHeaders,
        missing: config.required.headers,
      }))
    expect(() => getInstance(emptyHeaders))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingHeaders,
        missing: config.required.headers,
      }))
    expect(() => getInstance(emptyPassword))
      .toThrowCustomError(new ArgumentError(ERRORS.missingPassword))
    expect(() => getInstance(emptyUsername))
      .toThrowCustomError(new ArgumentError(ERRORS.missingUsername))
  })

  it('throws if `headers` option is invalid type', () => {
    const invalidHeaders = {
      headers: [],
      ...config.credentials.good,
    }

    expect(() => getInstance(invalidHeaders))
      .toThrowCustomError(new ArgumentError(ERRORS.missingHeadersOpt))
  })

  it('throws if any required headers are missing', () => {
    const missingHeaders = {
      headers: new Headers(goodOptions.headers),
      ...config.credentials.good,
    }

    missingHeaders.headers.delete('X-Plex-Version')

    expect(() => getInstance(missingHeaders))
      .toThrowCustomError(new ArgumentError({
        ...ERRORS.missingHeaders,
        missing: ['X-Plex-Version'],
      }))
  })

  it ('accepts Headers instance as `headers` option', () => {
    const options = {
      headers: new Headers(goodOptions.headers),
      ...config.credentials.good
    }

    expect(() => { getInstance(options) }).not.toThrow()
    expect(getInstance(options)).toBeInstanceOf(PlexAppAuthenticator)
  })

  it('returns a new plexAppAuthenticator instance', () => {
    const instance = getInstance(goodOptions)

    expect(instance).toBeInstanceOf(PlexAppAuthenticator)
  })
})

describe('plexAppAuthenticator instance', () => {
  const pAA = getInstance(goodOptions)

  it('exposes `options` property', () => {
    expect(pAA).toHaveProperty('options')
  })

  describe('`options` property', () => {
    it('provides username passed to constructor', () => {
      expect(pAA).toHaveProperty('options.username', goodOptions.username)
    })

    it('provides password passed to constructor', () => {
      expect(pAA).toHaveProperty('options.password', goodOptions.password)
    })
  })

  it('exposes `headers` property', () => {
    expect(pAA).toHaveProperty('headers')
  })

  describe('`headers` property', () => {
    it('is a `Headers` instance', () => {
      expect(pAA.headers).toBeInstanceOf(Headers)
    })

    it('provides all headers passed to constructor', () => {
      const headers = Object.keys(goodOptions.headers)

      expect.assertions(headers.length)
      for (let header of headers) {
        expect(pAA.headers.get(header))
          .toBe(goodOptions.headers[header])
      }
    })
  })

  it('exposes `token` property with empty String value', () => {
    expect(pAA).toHaveProperty('token', '')
  })

  it('exposes `getToken` method', () => {
    expect(typeof pAA.getToken).toBe('function')
  })


})

describe('plexAppAuthenticator `getToken` method', () => {
  let pAA = getInstance(goodOptions)

  afterEach(() => {
    pAA = getInstance(goodOptions)
    fetch.reset()
  })

  it('sends a POST request to API auth endpoint', async () => {
    fetch.post({
      url: config.request.url,
      response: config.responses.fakeToken,
      name: 'test-request',
    })

    expect.assertions(4)
    await expect(pAA.getToken()).resolves.toBeDefined()
    expect(fetch).toHaveFetched('test-request')
    expect(fetch).toBeDone('test-request')

    const url = fetch.calls('test-request')[0][0]

    expect(url).toBe(config.request.url)
  })

  it('throws on bad credentials', async () => {
    const pAA = getInstance({
      headers: goodOptions.headers, ...config.credentials.bad
    })

    fetch.post({
      url: config.request.url,
      response: config.responses.badCredentials,
      name: 'bad-credentials',
    })

    expect.assertions(3)
    await expect(pAA.getToken()).rejects.toThrow(APIError)
    expect(fetch).toHaveFetched('bad-credentials')
    expect(fetch).toBeDone('bad-credentials')
  })

  /*
   * Possible if Plex changes header requirements.
   * https://forums.plex.tv/t/how-to-request-a-x-plex-token-token-for-your-app/84551
   */
  it('throws on missing Plex-required headers', async () => {
    fetch.post({
      url: config.request.url,
      response: config.responses.badHeaders,
      name: 'missing-headers',
    })

    expect.assertions(3)
    await expect(pAA.getToken()).rejects.toThrow(APIError)
    expect(fetch).toHaveFetched('missing-headers')
    expect(fetch).toBeDone('missing-headers')
  })

  it('throws on missing token in API response', async () => {
    fetch.post({
      url: config.request.url,
      response: config.responses.badAPIToken,
      name: 'bad-token',
    })

    expect.assertions(3)
    await expect(pAA.getToken()).rejects.toThrow(APIError)
    expect(fetch).toHaveFetched('bad-token')
    expect(fetch).toBeDone('bad-token')
  })

  it('overwrites bad headers', async () => {
    const pAA = getInstance({
      headers: {
        ...config.request.headers.good,
        ...config.request.headers.extra,
      },
      ...config.credentials.good,
    })

    fetch.post({
      url: config.request.url,
      response: config.responses.fakeToken,
      name: 'bad-headers',
    })

    expect.assertions(Object.keys(config.request.headers.extra).length + 3)
    await expect(pAA.getToken()).resolves.toBeDefined()
    expect(fetch).toHaveFetched('bad-headers')
    expect(fetch).toBeDone('bad-headers')

    const request = fetch.calls('bad-headers')[0][1]

    for (let [header, value] of Object.entries(config.request.headers.extra)) {
      expect(request.headers.get(header)).not.toBe(value)
    }
  })

  it('resolves token on successful API response', async () => {
    fetch.post({
      url: config.request.url,
      response: config.responses.success,
      name: 'success',
    })

    expect.assertions(4)
    await expect(pAA.getToken()).resolves.toBe(config.token)
    expect(fetch).toHaveFetched('success')
    expect(fetch).toBeDone('success')
    expect(pAA.token).toBe(config.token)
  })

  describe('POST request', () => {
    let request

    beforeAll(async () => {
      fetch.post({
        url: config.request.url,
        response: config.responses.fakeToken,
        name: 'test-post',
      })

      await pAA.getToken()
      request = fetch.calls('test-post')[0][1]
    })

    afterAll(fetch.reset)

    test('`body` is instance of URLSearchParams', () => {
      expect(request.body).toBeInstanceOf(URLSearchParams)
    })

    test('URLSearchParams contains username and password', () => {
      expect(request.body.get(config.request.params.username))
        .toBe(goodOptions.username)
      expect(request.body.get(config.request.params.password))
        .toBe(goodOptions.password)
    })

    test('`headers` contains all headers stored in instance', () => {
      const headers = Object.keys(pAA.headers)

      expect.assertions(headers.length)
      for (let header of headers) {
        expect(request.headers.get(header))
          .toBe(pAA.headers.get(header))
      }
    })
  })
})
