'use strict'

//import fetch, { Headers } from 'node-fetch'
const { default: fetch, Headers } = require('node-fetch')

//import config from './config'
//import { APIError, ArgumentError, ERRORS } from './errors'
//const { AUTH_ENDPOINT, REQUIRED } = config
const { AUTH_ENDPOINT, REQUIRED } = require('./config')
const { APIError, ArgumentError, ERRORS } = require('./errors')

function extractToken (res) {
  const token = res.payload.user && res.payload.user.authToken
  if (token) { return Promise.resolve(token) }
  throw new APIError(res.status, res.statusText, 'Payload is missing token.')
}

async function parseAPIResponse (res) {
  const data = {
    payload: await res.json(),
    status: res.status,
    statusText: res.statusText,
  }
  if (res.ok) { return data }
  throw new APIError(res.status, res.statusText, data.payload.error)
}

function validateOptions (options) {
  let missing = []

  if (!(options && options.constructor === Object)) {
    throw new ArgumentError(ERRORS.missingOptionsArg)
  }
  missing = (
    opts => REQUIRED.options.filter(k => !opts.includes(k))
  )(Object.keys(options))
  if (missing.length > 0) {
    throw new ArgumentError({...ERRORS.missingOptions, missing})
  }
  const { headers: hdrs, password: pw, username: un } = options

  if (!(
    hdrs
    && (
      hdrs.constructor === Object
      || hdrs.constructor === Headers
  ))) {
    throw new ArgumentError(ERRORS.missingHeadersOpt)
  }
  missing = REQUIRED.headers.filter(
    h => hdrs.get ? !hdrs.get(h) : !(typeof hdrs[h] === 'string' && hdrs[h])
  )
  if (missing.length > 0) {
    throw new ArgumentError({...ERRORS.missingHeaders, missing})
  }

  if (!(typeof pw === 'string' && !!pw)) {
    throw new ArgumentError(ERRORS.missingPassword)
  }
  if (!(typeof un === 'string' && !!un)) {
    throw new ArgumentError(ERRORS.missingUsername)
  }

  return true
}

class PlexAppAuthenticator {
  constructor ({headers = {}, username = '', password = ''} = {}) {
    validateOptions(arguments[0])

    this.headers = new Headers(headers)
    this.options = {username, password}
    this.token = ''

    return this
  }

  getToken () {
    const params = new URLSearchParams

    this.headers.set('Accept', 'application/json')
    this.headers.set('Content-Type', 'application/x-www-form-urlencoded')
    params.set('user[login]', this.options.username)
    params.set('user[password]', this.options.password)

    return fetch(AUTH_ENDPOINT, {
      method: 'POST',
      body: params,
      headers: this.headers
    })
      .then(parseAPIResponse)
      .then(extractToken)
      .then(token => this.token = token)
  }
}

//export default PlexAppAuthenticator
//export { APIError, ArgumentError } from './errors'
module.exports = exports      = PlexAppAuthenticator
module.exports.default        = exports
module.exports.APIError       = APIError
module.exports.ArgumentError  = ArgumentError
