'use strict'

//import APIError from './APIError'
const APIError = require('./APIError')

describe('APIError', () => {
  it('is a class', () => {
    expect(typeof APIError.prototype.constructor).toBe('function')
    expect(APIError.prototype.constructor.name).toBe('APIError')
  })

  it('extends Error class', () => {
    expect(APIError.prototype instanceof Error).toEqual(true)
  })

  describe('APIError instance', () => {
    it('has `statusCode` property', () => {
      expect(new APIError(400)).toHaveProperty('statusCode', 400)
    })

    it('has `status` property', () => {
      expect(new APIError(403, 'Forbidden'))
        .toHaveProperty('status', 'Forbidden')
    })

    it('has `response` property', () => {
      expect(new APIError(500, 'Internal Server Error', 'response text'))
        .toHaveProperty('response', 'response text')
    })

    it('has `message` property', () => {
      expect(new APIError(1, 'status text', 'response text'))
        .toHaveProperty(
          'message',
          'Status: 1 status text, Response: response text'
        )
    })

    it('provides default properties', () => {
      const error = new APIError

      expect(error.statusCode).toBe(0)
      expect(error.status).toBe('')
      expect(error.response).toBe('')
      expect(error.message).toBe('Status: 0, Response: undefined')
    })

    it('throws with proper error message', () => {
      expect(_ => {
        throw new APIError(
          403, 'Verboten', 'Ungültiger Benutzername oder Passwort'
        )
      }).toThrowErrorMatchingSnapshot()
    })
  })
})
