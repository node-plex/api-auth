'use strict'

class APIError extends Error {
  constructor (statusCode = 0, status = '', response = '') {
    super()

    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)

    return Object.assign(this, {statusCode, status, response})
  }

  get message () {
    return `Status: ${this.statusCode}${this.status ? ` ${this.status}` : ''}`
      + `, Response: ${this.response ? this.response : 'undefined'}`
  }
}

//export default APIError
module.exports = APIError
