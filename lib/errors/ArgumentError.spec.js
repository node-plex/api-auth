'use strict'

//import ArgumentError from './ArgumentError'
const ArgumentError = require('./ArgumentError')

describe('ArgumentError', () => {
  it('is a class', () => {
    expect(typeof ArgumentError.prototype.constructor).toBe('function')
    expect(ArgumentError.prototype.constructor.name).toBe('ArgumentError')
  })

  it('extends Error class', () => {
    expect(ArgumentError.prototype instanceof Error).toEqual(true)
  })

  describe('ArgumentError instance', () => {
    it('has `code` property', () => {
      expect(new ArgumentError({code:'ERR_OUT_OF_RANGE'}))
        .toHaveProperty('code', 'ERR_OUT_OF_RANGE')
    })

    it('has `message` property', () => {
      expect(new ArgumentError({message:'message text'}))
        .toHaveProperty('message', 'message text')
    })

    it('has `missing` property', () => {
      expect(new ArgumentError({missing:['string']}))
        .toHaveProperty('missing', ['string'])
    })

    it('returns alternate message when missing array is not empty', () => {
      expect(new ArgumentError({
        code: 'ERR_MISSING_OPTION',
        message: 'Missing options:',
        missing: ['opt1']
      })).toHaveProperty('message', "Missing options: 'opt1'.")
    })

    it('provides default properties', () => {
      const error = new ArgumentError

      expect(error.code).toBe('ERR_INVALID_ARG_TYPE')
      expect(error.message).toBe('Invalid argument:')
      expect(error.missing).toEqual([])
    })

    it('throws with proper error message', () => {
      expect(_ => {
        throw new ArgumentError({
          code: 'ERR_MISSING_OPTION',
          message: 'Missing options:',
          missing: ['opt1', 'opt2']
        })
      }).toThrowErrorMatchingSnapshot()
    })
  })
})
