'use strict'

//import APIError from './APIError.js'
//import ArgumentError from './ArgumentError.js'
//import * as ERRORS from './errors.json'
const APIError = require('./APIError')
const ArgumentError = require('./ArgumentError')
const ERRORS = require('./errors.json')

/*
const ERRORS = Object.entries(errors).reduce(
  (o, [k, v]) => (o[k] = new ArgumentError(v.message, v.code), o), {}
)
*/

//export { APIError, ArgumentError, ERRORS }
module.exports = {
  APIError,
  ArgumentError,
  ERRORS,
}
