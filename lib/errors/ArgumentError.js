'use strict'

class ArgumentError extends Error {
  /** valid error codes
   * ERR_INVALID_ARG_TYPE
   * ERR_INVALID_ARG_VALUE
   * ERR_INVALID_OPT_VALUE
   * ERR_MISSING_OPTION
   * ERR_OUT_OF_RANGE
   */
  constructor ({
    code = 'ERR_INVALID_ARG_TYPE',
    message = 'Invalid argument:',
    missing = []
  } = {}) {
    super()

    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)

    return Object.assign(this, {code, message, missing})
  }

  get message () {
    if (!this.missing.length) { return this.preMessage }
    return `${this.preMessage} ${this.missing.map(k => `'${k}'`).join(', ')}.`
  }

  set message (message) {
    this.preMessage = message
  }
}

//export default ArgumentError
module.exports = ArgumentError
